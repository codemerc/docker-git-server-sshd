FROM registry.gitlab.com/codemerc/docker-sshd/sshd:python-2.7.15-slim-stretch

RUN apt-get update && apt-get -y install \
  git \
  && rm -rf /var/lib/apt/lists/*

COPY ./src/etc/ssh/sshd_config /etc/ssh/sshd_config-snip

RUN sed -i '/^session optional pam_umask.so umask=/s/umask=0117/umask=0022/' /etc/pam.d/sshd \
 && cat /etc/ssh/sshd_config-snip >>/etc/ssh/sshd_config \
 && rm -f /etc/ssh/sshd_config-snip

RUN usermod --login git user \
 && usermod --move-home --home /home/git git
COPY ./src/home/git/git-shell-commands/ /home/git/git-shell-commands/

COPY ./Dockerfile /Dockerfile
